--Creamos la base de datos
CREATE DATABASE dbcatalogo
GO
--Creamos un login para el usuario administrador
CREATE LOGIN admin WITH PASSWORD = 'adminnotiene'
GO
--Creamos un login para el usuario de lectura
CREATE LOGIN usuario WITH PASSWORD = 'notiene'
GO
--Cambiamos a la base de datos
USE dbcatalogo
GO
--Creamos los usuarios administrador y de sistema
CREATE USER admin FOR LOGIN admin;
CREATE USER usuario FOR LOGIN usuario;
--Agregamos el permiso al usuario administrador de db_owner el cual tiene acceso total a la base de datos
ALTER ROLE db_owner ADD MEMBER admin;
--Agregamos los roles de escritura y lectura para el usuario de sistema
ALTER ROLE db_datareader ADD MEMBER usuario;
ALTER ROLE db_datawriter ADD MEMBER usuario;