﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mscategoria.Modelos
{
    public class CatalogoContext: DbContext
    {
        public CatalogoContext(DbContextOptions<CatalogoContext> options)
                                              : base(options)
        {
        }
        public virtual DbSet<Categoria> Categoria { get; set; }
    }
}